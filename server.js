var express = require('express');

//custom routing for clientside
var routesIndex =  require('./routes/index');
var adminIndex = require('./routes/admin');
var accountIndex = require('./routes/account');
var profileIndex = require('./routes/profile');

//subdomain module for admin page
var subdomain = require('express-subdomain');

//sessions
var session = require('express-session');

//working path
var path = require('path');
//connection from connection-pool.js
var connection = require('./database/connection-pool');

//body parser to parse post data? not sure yet
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

var chalk = require('chalk');

//app usings
var app = express();

app.use(cookieParser());
app.use(bodyParser.urlencoded({
	extended: true
}))
app.use(cookieParser());
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: '#06C3729XSL98K',
    cookie: { expires: null }
}));
app.use(bodyParser.json());

//set view engine
app.set('view engine', 'ejs');
//set public folder for static files like JS/CSS
app.use(express.static(path.join(__dirname, 'public')));

//initial connect to the database
connection.connect((err) => {
	if (!err) {
		console.log(chalk.green('Connected to database... OK'));

	}
	else {
		console.log(chalk.red('Error connecting to database... Make sure MySQL service is running and the database exists.'));
	}
});

//All routes added in here. makes the app.use() function less crowded.
routesArray = [
		routesIndex, adminIndex, accountIndex,
		profileIndex];

app.use('/', routesArray);
app.use((req, res) => {
	res.render('pages/error')
});

//start the server on port 9191
app.listen(9191);
console.log('Server running at localhost:9191');