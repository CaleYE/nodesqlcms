var express = require('express');
var ejs = require('ejs');
var session = require('express-session');

//connection from connection-pool.js
var connection = require('../database/connection-pool');
var router = express.Router();
//changes console colour
//in the console, I want each GET/POST etc REQUEST to have a colour.
// GET = green
// POST = red
// NAMEOFPAGE = yellow. please see the app.get('/') below for the example of how to use.
var chalk = require('chalk');


var bodyParser = require('body-parser');
var urlEncodedParser = bodyParser.urlencoded({extended: false});

router.use(bodyParser.json());

// index page
router.get('/', (req, res) => {
	console.log(chalk.green('GET') + ' to / ' + chalk.yellow('(INDEX)'));

	var user = req.query['user'];
	sess = req.session;

	//sending data from db to view cyka
	connection.query('SELECT * FROM users', (err, result, fields) => {
			if (!err) {
				//This renders the file (pages/index) and it also gives the objects created (alcohol and tagline are static hardcoded while result[0].name is the result of the query
				res.render('pages/index',{
					singleName: (result[0].name).toString(),
					names: result,
					postUSER: user,
					loggedin: sess.loggedin,
					userfromsession: sess.yourname
				});
			}
	})
});

//about page
router.get('/about', (req, res) => {
	sess = req.session;
	console.log(chalk.green('GET') + ' to /about ' + chalk.yellow('(ABOUT)'));

	res.render('pages/about', {
		loggedin: sess.loggedin,
		userfromsession: sess.yourname
	});
})

router.get('/news',  (req, res) => {
	sess = req.session;

	console.log(chalk.green('GET'), 'to /news', chalk.yellow('(NEWS)'));

	connection.query('SELECT * FROM news_posts ORDER BY post_date DESC', (err, result) => {
		res.render('pages/news', {
			loggedin: sess.loggedin,
			userfromsession: sess.yourname,
			newsPosts: result
		})
	});
});

//ERROR PAGE this will be triggered whenever an error happens - still to be made into the core functionality
router.get('/error', (req, res) => {
	sess = req.session;

	console.log(chalk.green('GET') + ' to /error ' + chalk.red('(ERROR)'));
	res.render('pages/error', {
		loggedin: sess.loggedin,
		userfromsession: sess.yourname
	});
});

module.exports = router;