var express = require('express');
var ejs = require('ejs');
var session = require('express-session');
//connection from connection-pool.js
var connection = require('../database/connection-pool');
var profile = express.Router();

var cryptoEngine = require('../security/encryptor');

//changes console colour
//in the console, I want each GET/POST etc REQUEST to have a colour.
// GET = green
// POST = red
// NAMEOFPAGE = yellow. please see the app.get('/') below for the example of how to use.
var chalk = require('chalk');


var bodyParser = require('body-parser');
var urlEncodedParser = bodyParser.urlencoded({extended: false});
// index page
profile.use(bodyParser.json());


//profile page
profile.get('/user/profile', (req, res) => {
	sess = req.session;

	console.log(chalk.green('GET') + ' to /user/profile' + chalk.yellow('(PROFILE)'));
	//if you're logged in, the session variable called loggedin (sess.loggedin) is set to true. this is set in the login event. if its false, you get redirected to the login page
	if (sess.loggedin) {
		res.render('user/profile', {
			yourname: sess.yourname,
			userId: sess.userId,
			userRole: sess.userRole,
			email: sess.email
		});
	} else {
		res.redirect('/account/login');
	}
});

//edit page
profile.get('/user/edit', (req, res) => {
	sess = req.session;

	console.log(chalk.green('GET') + ' to /user/edit ' + chalk.yellow('(PROFILE)'));
	if (sess.loggedin) {
		res.render('user/edit', {
			yourname: sess.yourname,
			userId: sess.userId,
			email: sess.email
		});
	} else {
		res.redirect('/account/login');
	}
});


//Functional Edit page post
profile.post('/editProfile', (req, res) => {
	sess = req.session;

	console.log(chalk.red('POST') + ' to /editProfile ' + chalk.yellow('(PROFILE)'));

	var postVars = { username: req.body.name, email: req.body.email};
	var encryptedPassword = cryptoEngine.encrypt(req.body.pass);
	if (encryptedPassword == "") {
			connection.query('UPDATE users SET name="' + req.body.name + '", email="'+postVars.email+'" WHERE id = "' + sess.userId + '"',  (err, result) => {
			try{
				sess.yourname = req.body.name;
				sess.email = req.body.email;
				res.render('user/success/changed');
			} catch (e) {
				res.render('/error');
			}
		});	
	} else {
			connection.query('UPDATE users SET name="' + req.body.name + '", password="'+ encryptedPassword +'", email="'+postVars.email+'" WHERE id = "' + sess.userId + '"', (err, result) => {
			try{
				sess.yourname = req.body.name;
				sess.email = req.body.email;
				res.render('user/success/changed');
			} catch (e) {
				res.render('/error');
			}
		});	
	}
});

//ezpz delete user page
profile.get('/user/delete', (req, res) => {
	console.log(chalk.green('GET') + ' to /user/delete ' + chalk.yellow('(PROFILE)'));

	res.render('user/delete');
});

//post to delete the user
profile.post('/deleteProfile', (req, res) => {
	sess =  req.session;

	console.log(chalk.red('POST') + ' to /deleteProfile ' + chalk.yellow('(PROFILE)'));

	connection.query("DELETE FROM users WHERE id = '" + sess.userId + "'", (err, result) => {
		try{
			sess.destroy((err) => {
				if (err) {
					console.log(err);
				} else {
					console.log('User has been deleted: ', sess.yourname);
					res.redirect('/');
			}
			});
		} catch (e) {
			console.log('SUPER ERROR CYKA', e);
		}
	})
})

module.exports = profile;