var express = require('express');
var ejs = require('ejs');
var session = require('express-session');

//connection from connection-pool.js
var connection = require('../database/connection-pool');
var cryptoEngine = require('../security/encryptor');
var account = express.Router();
var bcrypt = require('bcrypt');


//changes console colour
//in the console, I want each GET/POST etc REQUEST to have a colour.
// GET = green
// POST = red
// NAMEOFPAGE = yellow. please see the app.get('/') below for the example of how to use.
var chalk = require('chalk');

var bodyParser = require('body-parser');
var urlEncodedParser = bodyParser.urlencoded({extended: false});

account.use(bodyParser.json());

account.get('/account/register', (req,res) => {
	console.log(chalk.green('GET') + ' to / ' + chalk.yellow('(REGISTER)'));
	res.render('account/register');
});


account.post('/doRegister' , urlEncodedParser, (req, res) => {
		console.log(chalk.red('POST') + ' to /somePage' + chalk.yellow('(REGISTER)'));
		if (req.body.registerbutton == "Register") {
		
			var postVars = { username: req.body.name, password: req.body.password, email: req.body.email };

			var encryptedPassword = cryptoEngine.encrypt(postVars.password);

			connection.query("INSERT INTO users (name, password, email) VALUES ('"+ postVars.username +"', '"+ encryptedPassword +"', '"+ postVars.email +"')", (err, result) => { //change query to: INSERT INTO users (name, password, etcetc) VALUES ?
				if(err) {
					res.render('account/registerAttempt', {
						error: "Username or email already exists!"
					});
				} else {
					console.log('registered user: ', postVars.username, encryptedPassword);
					res.redirect('/account/login');
				}
			});		
		}
});

//gets login page
account.get('/account/login', (req, res) => {
	console.log(chalk.green('GET') + ' to /login' + chalk.yellow('(LOGIN)'));

	res.render('account/login', {
		error: null
	});
});

//login functionality, this triggers from a post event TO /goLogin
account.post('/goLogin', (req, res) => {
	sess = req.session;

	console.log(chalk.red('POST') + ' to /goLogin' + chalk.yellow('(LOGIN)'));
	if (req.body.loginbutton == "Login") {		
		var post = { username: req.body.name, password: req.body.password };

		var encryptedPassword = cryptoEngine.encrypt(post.password);

		connection.query("SELECT * FROM users WHERE name = '"+post.username+"' AND password = '" + encryptedPassword + "'", (err, result) => {
		try {
			//is user logged in with correct details then proceed to set login
			if ((result[0].name).toString() != "") {
				// Session setters
				sess.loggedin = true;
				sess.yourname = post.username;
				sess.userId = (result[0].id).toString();
				sess.userRole = (result[0].role).toString();
				sess.email = (result[0].email).toString();

				console.log('User logged in: ', post.username, encryptedPassword, sess.userRole);
				res.redirect('/user/profile');
			}
		}
		catch(e) {
			//If user logged in with wrong details, this happens.
			sess.loggedin = false;
			res.render('account/login', {
				error: "Username and/or password incorrect. Try again."
			});
			console.log(chalk.red('user tried logging in with: ', post.username, encryptedPassword));
		}
	});	
	}
});

//Logout
account.get('/logout', (req, res) => {
	
	//kills session variables makes you logout
	req.session.destroy((err) => {
		if (err) {
			console.log(err);
		} else {
			console.log(chalk.green('GET') + ' to /logout' + chalk.yellow('(LOGOUT)'));
			res.redirect('/');
		}
	});
});

module.exports = account;