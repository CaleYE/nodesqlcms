var express = require('express');
var ejs = require('ejs');
var session = require('express-session');
//connection from connection-pool.js
var connection = require('../database/connection-pool');
var admin = express.Router();
//changes console colour
//in the console, I want each GET/POST etc REQUEST to have a colour.
// GET = green
// POST = red
// NAMEOFPAGE = yellow. please see the app.get('/') below for the example of how to use.
var chalk = require('chalk');


var bodyParser = require('body-parser');
var urlEncodedParser = bodyParser.urlencoded({extended: false});

admin.use(bodyParser.json());

//Gets admin index page.
admin.get('/admin', (req, res) => {
	sess = req.session;

	console.log(chalk.green('GET'), 'to /admin', chalk.yellow('(ADMIN)'));

	if (sess.userRole == 1) {
		connection.query("select * from users", (err, result, fields) => {
			if(!err) {
				res.render('admin/admin', {
					allUsers: result,
					loggedin: sess.loggedin,
					userfromsession: sess.yourname,
					userId: sess.userId
			});
		}
	});		
	} else {
		res.redirect('/');
	}
});

admin.get('/admin/pluginmanager', (req,res) => {
	sess = req.session;

	console.log(chalk.green('GET'), 'to /admin/managenews', chalk.yellow('(ADMIN)'));
	if (sess.userRole == 1) {
			res.render('admin/pluginmanager', {
			loggedin: sess.loggedin,
			userfromsession: sess.yourname,
			userId: sess.userId
		});	
	} else {
		res.redirect('/');
	}
});



///
/// Todo create new posts <- this only shows posts
///
admin.get('/admin/newsmanager', (req,res) => {
	sess = req.session;

	console.log(chalk.green('GET'), 'to /admin/newsmanager', chalk.yellow('(ADMIN)'));
	if (sess.userRole == 1) {
		connection.query('SELECT * FROM news_posts ORDER BY post_date DESC', (err, result) => {
			res.render('admin/newsmanager', {
				loggedin: sess.loggedin,
				userfromsession: sess.yourname,
				newsPosts: result
			})
		});
	} else {
		res.redirect('/');
	}
});

admin.post('/editpost', (req, res) => {
	sess = req.session;

	var postvars = { title: req.body.postTitle, postContent: req.body.postText, postid: req.body.postid }
	
	console.log(chalk.red('POST'), 'to /editpost', chalk.yellow('(ADMIN)'));
	if (sess.userRole == 1) {
		connection.query('UPDATE news_posts SET title="' + postvars.title + '", post="' + postvars.postContent + '" WHERE id = "' + postvars.postid + '"', (err, result) => {
			if (err) {
				console.log(chalk.red('ERROR: Could not write to database'));
				res.redirect('/')
			} else {
				res.redirect('/admin/newsmanager');
			}
		});
	} else {
		res.redirect('/');
	}
});

admin.post('/addpost', (req, res) => {
	sess = req.session;

	console.log(chalk.red('POST'), 'to /addpost', chalk.yellow('(ADMIN)'))

	var postvars= { title: req.body.newPostTitle, postContent: req.body.newPostText, datetoday: new Date().toISOString().split('T')[0] };

	if (sess.userRole == 1) {
		connection.query('INSERT INTO news_posts (title, post, post_date) VALUES ("'+ postvars.title +'","' + postvars.postContent + '","'+ postvars.datetoday +'")', (err, result) => {
			if (err) {
				console.log(chalk.red('ERROR: could not write to database\n', err))
				res.redirect('/')
			} else {
				res.redirect('/admin/newsmanager')
			}
		});
	} else {
		res.redirect('/');
	}

});

module.exports = admin;